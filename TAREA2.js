/*Creación de las variable*/
      const nombres = document.getElementById("nombre");
      const apellidos = document.getElementById("apellido");
      const cedulas = document.getElementById("cedula");
      const celulares = document.getElementById("celular");
      const emails = document.getElementById("email");
      const contraseñas = document.getElementById("contraseña");
      const recontraseñas = document.getElementById("recontraseña");
      const form = document.getElementById("ingreso");
      const listinputs = document.querySelectorAll(".ingreso_input");

      form.addEventListener("submit", (e) => {
          e.preventDefault();
          let condition = validacionForm();
          if (condition) {
              enviarForm();
          }
      });

      function validacionForm() {
          form.lastElementChild.innerHTML = "";
          let condition = true;
          listinputs.forEach((element) => {
              element.lastElementChild.innerHTML = "";
      }); 

      if (nombres.value.length  < 1 || nombres.value.trim() == "") {
        mostrarMensajeError("nombre", "El nombre ingresado no es correcto.*");
        condition = false;
       }

       if (apellidos.value.length  < 1 || apellidos.value.trim() == "") {
           mostrarMensajeError("apellido", "El apellido ingresado no es correcto.*"); 
           condition = false;           
        }

        if (
            cedulas.value.length != 10 || cedulas.value.trim() == "" ||
            isNaN(cedulas.value)  
        ){
            mostrarMensajeError("cedula", "La cédula ingresado no es valida*");
            condition = false; 
        }

        if (
            celulares.value.length != 10 || celulares.value.trim() == "" ||
            isNaN(celulares.value)  
        ){
            mostrarMensajeError("celular", "El celular ingresado no es valido*");
            condition = false; 
        }

        if (emails.value.length < 1 || emails.value.trim() == "" ) {
            mostrarMensajeError("email", "El email no es valida*");
            condition = false;
        }
        if (contraseñas.value.length < 1 || contraseñas.value.trim() == "") {
            mostrarMensajeError("contraseña", "Contraseña no valido*");
            condition = false;
        }
        if (recontraseñas.value != recontraseñas.value) {
            mostrarMensajeError("recontraseña");
            condition = false;
        }
        return condition;
     }
function mostrarMensajeError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
}
function enviarForm() {
    form.reset();
    form.lastElementChild.innerHTML = "Ingrese nuevos datos!!";
    alert("Sus datos fueron guardados");
}

